/* DOC-READY SCRIPTS (trigger when DOM has been built)
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------*/

$(document).ready(function() {

	/* LOAD LEXICON FILE
    ------------------------------------------------------------------------------*/
    var lang;
    var placeholders = {
        placeholder: 'some string from js',
        placeholder2: 'some other placeholder'
    };
    var data = placeholders;
    data['topic'] = "topicname";

    $.getJSON("lexicon.json", placeholders, function(data){
        lang = data;
        lexiconReady();
    });

    /* SCRIPTS AFTER LEXICON DONE
    ------------------------------------------------------------------------------*/

    function lexiconReady() {
        $(".placeholder").text(lang.test);
    }
});
