<?php
/**
 * Test Dutch lexicon topic
 *
 * @language nl
 */
$_lang['topicname.test'] = "Testopgave voor lexicon in nl";
$_lang['othertopic.test'] = "Testopgave in andere topic voor lexicon in nl";
$_lang['topicname.placeholder_test'] = "Testopgave voor lexicon in nl met placeholder '[[+placeholder1]]' en '[[+placeholder2]]'";
