<?php
/**
 * Test English lexicon topic
 *
 * @language en
 */
$_lang['topicname.test'] = "Test entry for lexicon in en";
$_lang['othertopic.test'] = "Test entry in other topic for lexicon in en";
$_lang['topicname.placeholder_test'] = "Test entry for lexicon in en with placeholder '[[+placeholder1]]' and '[[+placeholder2]]'";
