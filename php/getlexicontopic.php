<?php
/**
 * Parses a lexicon string, replacing placeholders with
 * specified strings.
 *
 * @access private
 * @param string $str The string to parse
 * @param array $params An associative array of keys to replace
 * @return string The processed string
 */
function parse($str,$params) {
    if (!$str) return '';
    if (empty($params)) return $str;

    foreach ($params as $k => $v) {
        $str = str_replace('[[+'.$k.']]',$v,$str);
    }
    return $str;
}

// check if it's a GET request
if ($_SERVER['REQUEST_METHOD'] != 'GET') {
    return 0;
}

// do a variable check
$topic = $_GET['topic'];
if (!isset($topic)) {
    $modx->log(modX::LOG_LEVEL_ERROR,'Topic not set in request; cannot retrieve lexicon for client-side using API.');
    return "please provide a topic";
}
$namespace = $_GET['ns'];
if (!isset($namespace)) $namespace = $modx->getOption('lexicon_namespace');
if (!$namespace) $namespace = "core";

// grab placeholders
$placeholders = $_GET;
unset($placeholder['topic']);

// do the lookup
$modx->getService('lexicon','modLexicon');
$modx->lexicon->load($namespace . ':' . $topic);
$lexi = $modx->lexicon->fetch($topic . '.', true); // retrieve entries + strip prefix

// process placeholders
$lexi_processed;
foreach ($lexi as $key => $value) {
    $lexi_processed[$key] = parse($value, $placeholders);
}

// return in json format
echo $modx->toJSON($lexi_processed);
