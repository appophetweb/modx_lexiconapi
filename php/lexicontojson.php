#!/usr/bin/php -q
<?php
    $flags = "p::o::";
    $prefix = "";
    $target = "lexicon.json";

    echo "Welcome to the lang-file to JSON convertor!\n";

    // check arguments
    $options = getopt($flags);
    print_r($options);
    if (count($argv) <= 1) exit(get_help());
    else if ($argv[1] == "help") exit(get_help());
    else if (count($options) + 1 >= count($argv)) exit(get_help()); // missing source path
    if ($options['p']) $prefix = $options['p'];
    if ($options['o']) $target = $options['o'];

    // load file
    require($argv[count($argv) - 1]); // load source
    if (!isset($_lang)) exit("lang file is empty or in wrong format");

    // filter + strip on prefix
    $lexi;
    foreach ($_lang as $key => $value) {
        if (!strncmp($key, $prefix, strlen($prefix))) { // is the prefix there?
            $strip = $prefix ? strlen($prefix) + 1 : 0;
            $lexi[substr($key, $strip)] = $value;
        }
    }

    // to json and writeout
    $json = json_encode($lexi);
    file_put_contents($target, $json);

    exit("\nfinished, see you soon!\n");

    function get_help() {
        $mssg = "This script converts a MODX PHP lang-file to a JSON file, useful for client-side development.\n\n" .
            "Usage: php lexicontojson.php (options) [source]\n" .
            "[source] is the path to the source lexicon file\n\n" .
            "options:\n" .
            "-o the output path to write the JSON to (default: lexicon.json)\n" .
            "-p prefix (topic) of lang keys to filter on and strip for output\n\n" .
            "Options and values should be concatenated, like: -ptopic, or -p=topic\n" .
            "PHP lexicon file should contain lexicon entries in the form: \$_lang['[topic].[keyname]'] = '[entry]';\n";
        return $mssg;
    }
?>
