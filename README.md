MODX Lexicon API
================

What is this?
-------------
A simple MODX Snippet and workflow to use the built-in lexicon client-side for more consistent and easier multi-language or internationalization (i18n) support.

How does it work?
-----------------
The built-in lexicon of MODX is used to store lexicon entries (file-based with optional database-backed override). Using a simple PHP script (snippet in MODX), a set of lexicon data based on the topic can be retrieved. A simple MODX resource with output type of JSON makes a call to the snippet and so creates a nice URL through which the little API becomes available (REST-ish). On the client side, one can post a simple AJAX request to get the lexicon entries.

Okay, I definitely want this, now what?
----------------------------
TODO for using MODX lexicon with the option to use it client-side:

- create a namespace with core path of i.e. *{base_path}assets/* (or a subdir of that in case of multi-site, as we like to do)
- create a dir *lexicon* in *{base_path}assets/* or copy from this repo
- create system or context setting *lexicon_namespace*, which indicates the namespace to use (you could also use the *core* namespace if you prefer that kind of style, optionally use the *ns* GET-variable to supply a namespace)
- create [topic].inc.php files for each lang, placed in a separate dir per lang
- for the topic name, keep in mind to use descriptive, unique names + use them in each lexicon key; i.e. *forms.name_tooshort*, in which *forms* is the topic name
- put the PHP-script as snippet in your MODX installation
- create a resource named something like *getlexicontopic*, with output format JSON, and call the snippet in the content (i.e. *![[getLexicon]]*). Make sure the resource is not cacheable and is hidden from menu

For the client-side (your JavaScripts), keep in mind that the API only reacts to GET-requests. An example of how to load the lexicon topic in your JS will be included in the near feature.

For added fun, use placeholders in you lexicon entries:

- placeholders in lexicon entries can be used in the form [[+placeholdername]]
- for the API, you can just provide GET-variables for a placeholder key, as in *fooplaceholder -> bar*
- the placeholders will be automatically replaced by the GET-variables, or at least that is the planned behavior (not yet functional)

Bonus material
--------------
The script *php/lexicontojson.php* converts a MODX PHP lang-file to a JSON file, useful for client-side development when the CMS is not available.

> Usage: php lexicontojson.php [source] ([target])
> [source] is the path to the source lexicon file
> [target] (optional) is the output path to write the JSON to (default: lexicon.json)

Or run `php lexicontojson.php help` for full help message. Planned for future development: support for multiple source files.
